package vn.kma.hrmactvn.service;


import java.sql.Timestamp;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import vn.kma.hrmactvn.controller.staff.dto.StaffCreateRequest;
import vn.kma.hrmactvn.entity.Staff;
import vn.kma.hrmactvn.error.ActvnException;
import vn.kma.hrmactvn.error.ExceptionMessage;
import vn.kma.hrmactvn.repository.StaffRepository;
import vn.kma.hrmactvn.utils.ApplicationUtils;

@RequiredArgsConstructor
@Service
public class StaffService {

  private final StaffRepository staffRepository;
  private final PasswordEncoder passwordEncoder;

  public Staff create(StaffCreateRequest request) throws ActvnException {
    Timestamp timestamp = new Timestamp(System.currentTimeMillis());

    boolean exists = staffRepository.existsByUsername(request.getUsername());

    if(exists){
      throw new ActvnException(HttpStatus.BAD_REQUEST.value(), ExceptionMessage.USERNAME_EXISTS);
    }

    Staff staff = Staff.builder()
        .username(request.getUsername())
        .password(passwordEncoder.encode(request.getPassword()))
        .level(request.getLevel())
        .createdBy("manhhd")
        .modifiedBy("manhhd")
        .createdDate(timestamp)
        .modifiedDate(timestamp)
        .jobTitle("ADMIN")
        .active(request.getActive())
        .build();
    return staffRepository.save(staff);
  }


  public Staff getMe() {
    System.err.println(ApplicationUtils.currentUser().getUsername());
    return new Staff();
  }
}
