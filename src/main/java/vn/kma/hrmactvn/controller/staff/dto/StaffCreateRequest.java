package vn.kma.hrmactvn.controller.staff.dto;

import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import vn.kma.hrmactvn.error.ExceptionMessage;

@Data
public class StaffCreateRequest {

  @NotNull(message = ExceptionMessage.USER_NAME_NULL)
  private String username;

  @NotNull(message = ExceptionMessage.PASSWORD_NULL)
  private String password;

  @NotNull(message = ExceptionMessage.LEVEL_NULL)
  private String level;

  @NotNull(message = ExceptionMessage.ACTIVE_NULL)
  private Boolean active;

  @NotNull(message = ExceptionMessage.JOB_TITLE_NULL)
  private String jobTitle;

  @Size(min = 10, max = 10, message = "Số điện thoại phải có 10 số")
  private String phoneNumber;

  @Email
  private String personalEmail;

  @Email
  private String workEmail;

  @NotNull(message = ExceptionMessage.FULL_NAME_NULL)
  private String fullName;

  @NotNull(message = ExceptionMessage.GENDER_NULL)
  @Digits(integer = 1, fraction = 0, message = "Giới tính chỉ có thể là 0 hoặc 1")
  private Integer gender;

  private String dateOfBirth;

  private Long rankId;

  private String identityBirthPlace;

  private String country;

  private String currentPlace;

  private String identityPlace; // Hộ khẩu thường trú

  private String identityCode;

  private String identityDate;

  private String placeOfIssue;

  private String favorite;



}
