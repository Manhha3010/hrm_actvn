package vn.kma.hrmactvn.constant;

public interface ApplicationConstant {
  public interface SECURITY_CONFIG {

    String[] DISABLE_AUTHORIZE = {
        "/auth/**",
        "/public/**",
        "/error/**",
    };
  }
}
