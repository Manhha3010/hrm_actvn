package vn.kma.hrmactvn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.kma.hrmactvn.entity.StaffContract;

public interface StaffContractRepository extends JpaRepository<StaffContract, Long> {

}
