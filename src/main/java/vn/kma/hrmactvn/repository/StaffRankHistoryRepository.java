package vn.kma.hrmactvn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.kma.hrmactvn.entity.StaffRankHistory;

public interface StaffRankHistoryRepository extends JpaRepository<StaffRankHistory, Long> {

}
