package vn.kma.hrmactvn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.kma.hrmactvn.entity.StaffInfo;

public interface StaffInfoRepository  extends JpaRepository<StaffInfo, Long> {

}
