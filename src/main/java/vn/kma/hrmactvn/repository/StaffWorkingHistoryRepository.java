package vn.kma.hrmactvn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.kma.hrmactvn.entity.StaffWorkingHistory;

public interface StaffWorkingHistoryRepository extends JpaRepository<StaffWorkingHistory, Long> {

}
