package vn.kma.hrmactvn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.kma.hrmactvn.entity.RankConfig;

public interface RankConfigRepository extends JpaRepository<RankConfig, Long> {

}
