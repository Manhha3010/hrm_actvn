package vn.kma.hrmactvn.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.kma.hrmactvn.entity.StaffAdmission;

public interface StaffAdmissionRepository extends JpaRepository<StaffAdmission, Long> {

}
